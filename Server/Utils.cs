// Utils script. Put useful static utility methods here
using Godot;
using System;
using Godot.Collections;

public static class Utils
{

    public static string GetUniqueName(System.Collections.Generic.ICollection<string> nameList, string name)
    {
        Random rand = new Random();
        string lobbyName = name;

        if (nameList.Contains(lobbyName))
        {
                lobbyName = lobbyName + " " + rand.Next(1000).ToString();
                return GetUniqueName(nameList,lobbyName);
        }
        return lobbyName;
    }


    public static Array<T> MonoListToGArray<T>(System.Collections.Generic.List<T> monoList)
    {
        Array<T> gArray = new Array<T>();
        foreach (T item in monoList)
           gArray.Add(item);
        
        return gArray;
    }    
    
}
