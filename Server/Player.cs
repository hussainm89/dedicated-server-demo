// Player script. Fairly straightforward.
// Only networking code is managing Rpc calls from input by client-controlled players.
// Compute's position based on input from player in client. 
// Handles logic (input received in client nodes)
// Note player syncing mainly done in Game as all players need to be sync'd for all clients
using Godot;
using System;
using System.Collections.Generic;

public class Player : Area2D
{

    public delegate void AttackInputDelegate(List<int> playerIDs);
    public event AttackInputDelegate AttackInput;

    public int OwnerID;
    public string PlayerName;
    private float _speed = 200;
    private Vector2 _velocity = new Vector2(0,0);
    private List<int> _playerIDsToAttack = new List<int>();

    public override void _Ready()
    {
        
    }

    public override void _PhysicsProcess(float delta)
    {
        SetPosition(GetPosition() + (_velocity * delta));
        
    }

    public void Die()
    {
        QueueFree();
    }

    private void _on_AreaAttack_area_entered(Area2D area)
    {
        if (! (area is Player) || area == this)
            return;
        Player p = (Player) area;
        if (! _playerIDsToAttack.Contains(p.OwnerID))
            _playerIDsToAttack.Add(p.OwnerID);
    }


    private void _on_AreaAttack_area_exited(Area2D area)
    {
        if (! (area is Player) || area == this)
            return;
        Player p = (Player) area;
        if (_playerIDsToAttack.Contains(p.OwnerID))
            _playerIDsToAttack.Remove(p.OwnerID);
    }

    // accessed by player _PhysicsProcess - so every frame
    [Remote]
    private void RequestMoveInput(Vector2 moveDir)
    {
        _velocity = moveDir.Normalized() * _speed;
    }

    [Remote]
    private void RequestAttackInput()
    {
        AttackInput?.Invoke(_playerIDsToAttack);        
    }

    // dummy methods used by client
    public virtual void SetNewPosition(Vector2 newPos)
    {

    }
}