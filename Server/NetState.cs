// NetState script. This handles main server networking code that generally needs to be globally accessible.
// This is always at /root/NetState
// Ideally, code that can be in other nodes (e.g. Lobby) should be in there rather than here
// As this uses a dedicated server model, we generally use [Remote] for simplicity
// As methods differ between server and client, we make "dummy" methods to help keep track of things (optional)
// We don't make use of SetNetworkMaster(peerID). Should we explicity make server master of all nodes?

//  **** NETWORKING NOTES for reference ****
//    See: http://docs.godotengine.org/en/latest/tutorials/networking/high_level_multiplayer.html
//
//    ** Initialising as a peer **
//    To initialise as peer we must:
//    Create a host var: NetworkedMultiplayerENet host = new NetworkedMultiplayerENet();
//    Set as server or client: host.CreateServer or host.CreateClient
//    Set as network peer to handle remote procedure calls: GetTree().SetNetworkPeer(host);
//    
//    ** Remote procedure calls **
//    These allow a peer to access vars and methods in remote peers
//    Rpc sends a remote procedure call (to all peers)
//    RpcId sends a remote procedure call to the peer with the specified id
//    Rset changes a variable for a remote peer. RsetId for peer with specified id.
//    The above all work via TCP (slow, reliable). 
//    To work over UDP (fast, unreliable) must append unreliable e.g. RpcUnreliable. This is important for time-critical processes e.g. physics
//    Note we can only send variant types as args via Rpc (see https://github.com/godotengine/godot/blob/master/core/variant.h line 75)
//    Even sending objects and trying to recast fails, so only send variants otherwise will get e.g. InvalidCastException
//    Despite this, we can still sync player data in a PlayerData class (methods that translate to and from variants to custom data objects)
//    Note a node must be within the scene tree to receive remote procedure calls
//
//    ** Rpc and attributes **
//    For Rpcs to work, a method or var needs to have an attribute out of: [Remote], [Sync], [Master], [Puppet]
//    Rpc and RpcId behave differently depending on these attributes
//    [Remote]: will execute on remote peers only
//    [Sync]: will execute both locally and on remote peers
//    [Master]: will execute for the peer that has authority over the node.
//    [Puppet]: will execute for peers that do not have authority over the node
//    The master of a node is set by node.SetNetworkMaster (which by default works recursively - on child nodes)
//    NOTE in a recent patch Slave was changed to Puppet
//

using Godot;
using System;
// We use Godot.Collections as we send dictionary to clients and this provides variant type (System.Collections.Generic.Dictionary is not)
using Godot.Collections;

public class NetState : Node
{

    private const int PORT = 5000;
    // If this is scaled, when MAX_PLAYERS is reached we would tell client to query a different IP (different server)
    private const int MAX_PLAYERS = 200;
    private int _playersPerMatch = 3;

    private Dictionary<int, Dictionary<string, object>> _connectedPlayers = new Dictionary<int, 
    Dictionary<string, object>>();
    private Dictionary<string,string> _lobbies = new Dictionary<string,string>();

    #region matchmaking variables
    // To make this simpler, it would be a good idea to make a separate matchmaking script
    private System.Collections.Generic.List<int> _matchQueuedPlayers = new System.Collections.Generic.List<int>();
    private System.Collections.Generic.List<int> _matchQueuedCandidates = new System.Collections.Generic.List<int>();
    // Create a timer for matchmaking checks with interval of 3 seconds
    private System.Timers.Timer _matchmakingTimer;
    private double _matchmakingTimerInterval = 3000.0;
    private int _startSearchRange = 5;
    private int _searchRange = 5;
    private int _maxSearchRange = 25;
    #endregion

    public override void _Ready()
    {
        // Initialise as peer and create server
        NetworkedMultiplayerENet server = new NetworkedMultiplayerENet();
        server.CreateServer(PORT, MAX_PLAYERS);
        GetTree().SetNetworkPeer(server);  

        // Connect some of the signals in SceneTree to manage connections to the server
        GetTree().Connect("network_peer_connected", this, nameof(OnClientConnected));
        GetTree().Connect("network_peer_disconnected", this, nameof(OnClientDisconnected));

        // Start timer to handle matchmaking
        _matchmakingTimer = new System.Timers.Timer(_matchmakingTimerInterval);
        _matchmakingTimer.Elapsed += new System.Timers.ElapsedEventHandler(OnMatchmakingTimerElapsed);
        _matchmakingTimer.AutoReset = true;
        _matchmakingTimer.Start();
    }

    // We add the connected client to our dict of connected players
    // We need to store info for each player - their lobby name (if they host) and their player name
    // If this becomes more complex we would create a PlayerData object with data that needs to be sent between peers
    // This would need to be translated to and from variant with a method before sending
    private void OnClientConnected(int id)
    {
        Random rand = new Random();
        GD.Print("Client ", id, " connected to Server");
        _connectedPlayers.Add(id, 
        new Dictionary<string, object> {{"lobby", "0"}, {"name", "MyName"}, {"mmr", rand.Next(0,20)}});
    }

    // When a client disconnects we need to manage this in the game.
    // We remove the connected player from our dict, remove player from any lobbies they were in,
    // and if the player was the last one in the lobby, we delete the lobby
    private void OnClientDisconnected(int id)
    {
        if (! _connectedPlayers.ContainsKey(id))
            return;
            
        if ((string)_connectedPlayers[id]["lobby"] != "0")
        {
            Lobby playerLobby = GetTree().GetRoot().GetNode<Lobby>((string)_connectedPlayers[id]["lobby"]);
            if (playerLobby.RemovePlayer(id) == 0)
                _lobbies.Remove((string)_connectedPlayers[id]["lobby"]);
        }
        _connectedPlayers.Remove(id);
        if (_matchQueuedPlayers.Contains(id))
            _matchQueuedPlayers.Remove(id);
        if (_matchQueuedCandidates.Contains(id))
            _matchQueuedCandidates.Remove(id);      
    }

    #region lobby specific methods

    // Get a list of lobbies flagged as joinable and send it to network peers
    private void UpdateClientLobbyList()
    {
        Rpc(nameof(LobbyListRequestOk), GetJoinableLobbies());
    }

    // Filter lobbies dict into lobbies that are joinable (there is an easier way to do this)
    private Dictionary<string,string> GetJoinableLobbies()
    {
        Dictionary<string,string> lobbies = new Dictionary<string,string>();
    
        foreach (string lobbyStr in _lobbies.Keys)
        {
            Lobby lobby = GetTree().GetRoot().GetNode<Lobby>(lobbyStr);
            if (lobby.Joinable == true)
                lobbies[lobbyStr] = _lobbies[lobbyStr];
        }
        return lobbies;
    }

    // Triggered by LobbiesChanged event to update lobby list in peers
    private void OnLobbiesChanged()
    {
        UpdateClientLobbyList();
    }

    // These are methods flagged as [Remote]
    // Client peers send Rpcs to server
    // I find it easier to suffix with Request to remind me the client requested from the server
    
    // When a client tries to host a game, it calls this method via Rpc  
    // This method does some basic error checking, then initialises the lobby (set name etc),
    // and deals with issues such as duplicate lobby names
    // Then confirms to client that the request was successful, 
    // and updates the connectedPlayer dict to show that this client is in this lobby
    // and updates the connectedPlayer dict to show the player's name (as the player picks the name before
    // making or joining the lobby)
    // This can be split into a few different methods to improve readability
    [Remote]
    private void HostRequest(int id, string hostLobbyName)
    {
        if (GetTree().GetRoot().HasNode("Lobby" + id.ToString()))
        {
            string reason = string.Format("lobby with id {0} already exists", id);
            GD.Print("Hosting lobby failed: ", reason);
            RpcId(id, nameof(HostRequestFailed), reason);
            return;
        }

        PackedScene lobbyScn = (PackedScene) GD.Load("res://Lobby.tscn");
        Lobby lobby = (Lobby) lobbyScn.Instance();
        lobby.SetName("Lobby" + id.ToString());
        lobby.HostID = id;
        GetTree().GetRoot().AddChild(lobby);

        // make sure lobby name is unique
        string lobbyName = Utils.GetUniqueName(_lobbies.Values, hostLobbyName);
        _lobbies[lobby.Name] = lobbyName;
        lobby.LobbyName = lobbyName;

        // After host request ok lobby is added to client tree
        RpcId(id, nameof(HostRequestOk));

        _connectedPlayers[id]["lobby"] = lobby.Name;
        // Note we set the player's name using a method that returns a new name if needed to avoid duplicates
        // This adds the player to server lobby and all player's lobbies as well
        // We should split this into two methods as it is bad practice for one method to do more than one job
        _connectedPlayers[id]["name"] = lobby.AddNewPlayer(id, _connectedPlayers[id]);

        lobby.LobbiesChanged += this.OnLobbiesChanged;

        UpdateClientLobbyList();
    }

    // When a client tries to join a game, it calls this method via Rpc
    // This method again does basic error checking, then finds a reference to the requested lobby,
    // Then checks if it is joinable, and if not (e.g. game is ongoing) returns with an error
    // If no issues, confirms with client that joining was successful, and updates the player info
    // in the connectedPlayers dict
    [Remote]
    public void JoinRequest(int id, string lobbyName)
    {
        // error checking
        if (! _lobbies.ContainsKey(lobbyName))
        {
            GD.Print("Lobby not found");
            return;
        }

        Lobby lobby = GetTree().GetRoot().GetNode<Lobby>(lobbyName);

        if (! lobby.Joinable)
        {
            GD.Print("Lobby is not joinable!");
            return;
        }

        RpcId(id, nameof(JoinRequestOk), lobbyName);

        _connectedPlayers[id]["name"] = lobby.AddNewPlayer(id, _connectedPlayers[id]);
        _connectedPlayers[id]["lobby"] = lobbyName;

    }

    // When the client tries to request the list of lobbies it calls this method via RPC
    [Remote]
    public void LobbyListRequest(int id)
    {
        RpcId(id, nameof(LobbyListRequestOk), GetJoinableLobbies());
    }

    // When a client wants to change player name
    [Remote]
    public void ChangeNameRequest(string name)
    {
        int ID = GetTree().GetRpcSenderId();
        _connectedPlayers[ID]["name"] = name;
    }

    #endregion

    #region matchmaking methods
    // When the client requests to find a match, we put them in the queue to find a match
    [Remote]
    private void FindMatchRequest()
    {
        int senderID = GetTree().GetRpcSenderId();

        if (! _matchQueuedPlayers.Contains(senderID))
            _matchQueuedPlayers.Add(senderID);
    }

    // If the client aborts, we remove them from the matchmaking lists
    [Remote]
    private void StopFindMatchRequest()
    {
        int senderID = GetTree().GetRpcSenderId();

        if (_matchQueuedPlayers.Contains(senderID))
            _matchQueuedPlayers.Remove(senderID);
        if (_matchQueuedCandidates.Contains(senderID))
            _matchQueuedCandidates.Remove(senderID);
    }


    // Called when matchmaking timer elapse event is raised. Runs every n (3) seconds. Decrease this if matchmaking too slow despite enough players
    // Loops through all queued players and compares player MMR (matchmaking rating) with each other, and adds similar MMR to a candidate list
    // If not enough candidates, stops and this method will run again after elapsed time with an expanded search range.
    // Remove any excess players from candidate list if too many added
    // Then if enough players in candidate list, reset the search range and commence the match.
    // Perhaps make asynchronous method if causes server to hang.
    private void OnMatchmakingTimerElapsed(object source, System.Timers.ElapsedEventArgs e)
    {
        // Loop through queued players. Take a candidate from the queued players (cID)
        foreach (int cID in _matchQueuedPlayers)
        {
            // Clear the candidate list (only players of similar MMR should be added)
            _matchQueuedCandidates.Clear();

            int cMMR = (int) _connectedPlayers[cID]["mmr"];
            
            // Loop through all other queued players
            foreach (int pID in _matchQueuedPlayers)
            {
                if (pID == cID)
                    continue;
                // Get each other queued player's MMR and compare with candidate
                int pMMR = (int) _connectedPlayers[pID]["mmr"];

                // If within range of the candidate MMR, add to the candidate list
                if (Math.Abs(cMMR - pMMR) < _searchRange)
                {
                    if (! _matchQueuedCandidates.Contains(pID) )
                    {
                        _matchQueuedCandidates.Add(pID);
                    }
                    if ( ! _matchQueuedCandidates.Contains(cID))
                    {
                        _matchQueuedCandidates.Add(cID);
                    }
                }

                // If we have enough candidates, break the loop
                if (_matchQueuedCandidates.Count >= _playersPerMatch)
                    break;
            }
            
            // Need to break here as well to avoid reseting the candidate list
            if (_matchQueuedCandidates.Count >= _playersPerMatch)
                    break;
        }

        // Expand search range if we don't find enough candidates
        if (_matchQueuedCandidates.Count < _playersPerMatch)
        {
            _searchRange = Math.Min(_searchRange + 1, _maxSearchRange);
            return;
        }

        // Remove excess players from candidate list
        while (_matchQueuedCandidates.Count > _playersPerMatch)
        {
            _matchQueuedCandidates.RemoveAt( (new Random()).Next(0,_matchQueuedCandidates.Count));
        }

        // If we do have enough candidates, reset the search range and commence the match
        _searchRange = _startSearchRange;
        GD.Print(_playersPerMatch + " players found! Starting match...");
        StartMatchmakingMatch();
        

    }

    // When enough candidates in the candidate list, the match will commence. Utilises existing lobby system
    // Makes one player the host and others join the lobby which is hidden from the player (as lobby UI not needed here)
    // Remove players in match from pool searching and from candidate list
    // Also tells each player who joins that find match request was successful
    private void StartMatchmakingMatch()
    {
        HostRequest(_matchQueuedCandidates[0], "Match Lobby");
        RpcId(_matchQueuedCandidates[0], nameof(FindMatchRequestOk));

        foreach (int pID in _matchQueuedCandidates)
        {
            _matchQueuedPlayers.Remove(pID);

            if (_matchQueuedCandidates.IndexOf(pID) == 0)
                continue;
            JoinRequest(pID, (string)_connectedPlayers[_matchQueuedCandidates[0]]["lobby"]);
            RpcId(pID, nameof(FindMatchRequestOk));
        }
        Lobby l = GetTree().GetRoot().GetNode<Lobby>((string)_connectedPlayers[_matchQueuedCandidates[0]]["lobby"]);
        l.Joinable = false;
        l.StartGameAll();
        _matchQueuedCandidates.Clear();
    }
    #endregion

    // Dummy methods used by client
    public virtual void HostRequestOk()
    {
        
    }
    public virtual void JoinRequestOk()
    {
        
    }
    public virtual void HostRequestFailed()
    {
        
    }
    
    public virtual void LobbyListRequestOk()
    {

    }
    public virtual void FindMatchRequestOk()
    {

    }

}
