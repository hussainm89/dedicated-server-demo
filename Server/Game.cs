// Game script. Runs the example game used to demonstrate multiplayer with multiple lobbies
// Initialised by Start(player list) in the Lobby parent node. This is easier to keep track of than _Ready,
// which runs when it enters the SceneTree (using Rpc calls in _Ready caused some errors)

using Godot;
using System;
using System.Collections.Generic;

public class Game : Node2D
{

    private PackedScene _playerScn;
    private Random rand = new Random();
    private Godot.Collections.Dictionary<int, Godot.Collections.Dictionary<string, object>> _players;

    public override void _Ready()
    {
        _playerScn = (PackedScene) GD.Load("res://Player.tscn");
        // We don't want to process physics until the Start method has been called
        // Otherwise the script won't find players as soon as this enters the scenetree
        SetPhysicsProcess(false);
    }

    private Vector2 RandomVector2(int min = 0, int max = 1000)
    {
        return new Vector2((float)rand.Next(min, max), (float)rand.Next(min, max));
    }

    // For every player connected to this game, generate a player and get it's position
    // Synchronise each generated player with all peers in the lobby
    public void Start(Godot.Collections.Dictionary<int, Godot.Collections.Dictionary<string, object>> players)
    {
        _players = players;
        foreach (int playerID in _players.Keys)
        {
            
            Vector2 playerPos = GeneratePlayer(playerID);

            // After the player is generated (i.e. before continuing the loop) loop through all players and give them this new player and position
            foreach (int ID in _players.Keys)
            {
                RpcId(ID, nameof(GeneratePlayer), playerID, playerPos, _players[playerID]["name"]);
            }
        }
        SetPhysicsProcess(true);
    }

    // Most important here is setting the unique Name of the player node, and the player owner ID, important for networking
    // We also set the visible name selected by the player, and connect events
    // Return the position as this is generated randomly then needs to be synced to all players
    private Vector2 GeneratePlayer(int ID)
    {
        Player player = (Player) _playerScn.Instance();
        player.Name = "Player" + ID.ToString();
        player.OwnerID = ID;
        AddChild(player);
        player.SetPosition(RandomVector2(0,500));
        player.PlayerName = (string)_players[ID]["name"];
        player.AttackInput += OnAttackInput;
        return player.GetPosition();
    }

    // When a player is removed in Lobby, it uses this method to remove from Game._players and run the Player.Die() method
    // We tell other players in the lobby to kill the player as well
    public void KillPlayer(int ID)
    {
        string playerName = "Player" + ID.ToString();
        GetNode<Player>(playerName).Die();
        _players.Remove(ID);

        // Remove player for each client
        // don't include removed player in rpcid call because this happens after they disconnect.. modify this if other criteria exist
        foreach (int playerID in _players.Keys)
        {
            RpcId(playerID, nameof(KillPlayerFromServer), playerName);
        }
    }

    // Every frame, the server synchronises position to each client (as this is a real-time game and positions change every frame)
    // I thought about using OnPositionChanged events client-side to signal server a requested change in position
    // But this is probably faster
    // In a turn-based game, would not need to do this (would send less packets as a result)
    // In more complex games, especially using physics, would need client-side prediction and latency management etc.
    // https://www.gabrielgambetta.com/client-server-game-architecture.html
    public override void _PhysicsProcess(float delta)
    {
        if (_players == null)
            return;
        // sync all players to each client
        foreach (int playerID in _players.Keys)
        {
            string playerNode = "Player" + playerID.ToString();
            Player player = GetNode<Player>(playerNode);
            
            Vector2 playerPos = player.GetPosition();
            foreach (int ID in _players.Keys)
            {
                RpcId(ID, nameof(SyncPlayerPosFromServer), playerNode, playerPos);
            }

        }
    }

    // When the player hits the attack key, an event is raised which asks the server to handle attack logic
    // Currently we simply change the colour of nearby players, then sync the player colours to all clients in the lobby
    // This gets messy with physics, as the server detects overlapping bodies in DIFFERENT games that have same position
    // We have to explicitly exclude these other bodies (a bit of a hack) - any alternative solution?
    private void OnAttackInput(List<int> playerIDs)
    {
        foreach (int playerID in playerIDs)
        {
            if (! _players.ContainsKey(playerID))
                continue;

            Color newColour = new Color((float)rand.NextDouble(),(float)rand.NextDouble(),(float)rand.NextDouble());
            string playerNode = "Player" + playerID.ToString();
            GetNode<Player>(playerNode).SetModulate(newColour);

            foreach (int ID in _players.Keys)
            {
                RpcId(ID, nameof(SyncPlayerColourFromServer), playerNode, newColour);
            }
        }
    }

    // dummy methods used by client
    public virtual void SyncPlayerPosFromServer(string playerNode, Vector2 newPos)
    {

    }
    public virtual void SyncPlayerColourFromServer(string playerNode, Color colour)
    {

    }
    public virtual void KillPlayerFromServer(string playerNode)
    {

    }
}
