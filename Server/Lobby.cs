// Lobby script. Manages the lobby. A lobby is created when a client successfully requests to host a lobby.
// The lobby is actually managed by the server but the host gets some special privileges (for now just starting the game).
// The server can manage many unique lobbies (max players in server / max players in lobby) e.g. 200/8 = 25
using Godot;
using System;
using Godot.Collections;

public class Lobby : Node
{

    public delegate void LobbiesChangedDelegate();
    public event LobbiesChangedDelegate LobbiesChanged;

    // List of players in this lobby only
    public Dictionary<int, Dictionary<string, object>> Players {get; set;} = new Dictionary<int, Dictionary<string, object>>();
    public int HostID {get; set;}
    public bool Joinable {get; set;} = true;
    public string LobbyName = "MyLobby";

    public override void _Ready()
    {
        
    }

    // Called by NetState when player successfully hosts or joins a lobby
    // Should be split into three methods as this currently: 
    // makes a player's name unique, adds player to server lobby list, and 
    // synchronises with other players in the lobby
    public string AddNewPlayer(int ID, Dictionary<string,object> playerData)
    {

        System.Collections.Generic.List<string> nameList = new System.Collections.Generic.List<string>();
        foreach (Dictionary<string,object> pData in Players.Values)
        {
            nameList.Add((string)pData["name"]);
        }

        Players[ID] = playerData;
        Players[ID]["name"] = Utils.GetUniqueName(nameList, (string)Players[ID]["name"]);

        // messy loops to add all player ids to all players -refactor when get time
        foreach (int playerID in Players.Keys)
        {
            foreach (int pID in Players.Keys)
            {
                RpcId(playerID, nameof(AddPlayer), pID, Players[pID]["name"]);
            }
        }

        return (string)Players[ID]["name"];
    }

    // Removes player from the lobby. If it is the last player, return 0
    // (this signals that the lobby is killed so it can be removed from list in NetState)
    public int RemovePlayer(int ID)
    {
        Players.Remove(ID);
        
        if (HasNode("Game"))
        {
            GetNode<Game>("Game").KillPlayer(ID);
        }

        foreach (int playerID in Players.Keys)
        {
            RpcId(playerID, nameof(RemovePlayer), ID);
        }

        if (Players.Count == 0)
        {
            GD.Print("Player count for " + this.Name + " is 0. Killing lobby.");
            QueueFree();
            LobbiesChanged?.Invoke();
            return 0;
        }
        return 1;
    }

    // Called when client requests to start the game (see below)
    // Adds the game scene as child of lobby and initialises the game with the list of players
    // Prevents further players from joining and signals to NetState that the lobby list has changed
    private void StartGame()
    {
        PackedScene gameScn = (PackedScene) GD.Load("res://Game.tscn");
        Game game = (Game) gameScn.Instance();
        AddChild(game);
        game.Start(Players);
        Joinable = false;
        LobbiesChanged?.Invoke();
    }

    // Called when client requests to start the game, does basic error checking, 
    // then sends Rpc call to each player in the lobby to start the game, and starts the game server-side as well
    [Remote]
    private void StartRequest()
    {
        int senderID = GetTree().GetRpcSenderId();
        if (senderID != HostID)
        {
            GD.Print("Only host can start game.");
            return;
        }
        StartGameAll();
    }

    public void StartGameAll()
    {


        foreach (int playerID in Players.Keys)
        {
            GD.Print(playerID);
            GD.Print(Players.Count);
            RpcId(playerID, nameof(StartGame));
        }
        StartGame();
    }


    // Dummy methods used by client 
    public virtual void AddPlayer(int ID)
    {

    }

}
