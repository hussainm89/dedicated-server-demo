// Main menu script. Placeholder to show how multiplayer can fit into a game.
// For other modes e.g. Single Player or Local, maybe use FSM to minimise code duplication
using Godot;
using System;

public class MainMenu : Control
{
    private NetState _netState;

    public override void _Ready()
    {
        GetNode<Panel>("PnlFindMatch").Hide();
    }


    private void NotImplementedError()
    {
        GetNode<WindowDialog>("WindowError").Show();
    }

    private void _on_BtnSinglePlayer_pressed()
    {
        NotImplementedError();
    }


    private void _on_BtnMultiplayer_pressed()
    {
        GetNode<WindowDialog>("WindowMultiplayer").Show();
    }

    private async void _on_BtnFindMatch_pressed()
    {
        // When find match is pressed initialise networking (we could also make a separate FindMatch scene and do it there)
        PackedScene netStateScn = (PackedScene)GD.Load("NetState.tscn");
        _netState = (NetState) netStateScn.Instance();
        GetTree().GetRoot().AddChild(_netState);
        _netState.MatchmakingGameStarted += this.OnMatchmakingGameStarted;

        // Wait until we're connected (e.g. play animation here)
        while (GetTree().GetNetworkPeer().GetConnectionStatus() != NetworkedMultiplayerPeer.ConnectionStatus.Connected)
        {
            // not yet connected
            await ToSignal(GetTree(), "idle_frame");
        }
        // Ask the server to find match
        _netState.RpcId(1, nameof(_netState.FindMatchRequest));//, GetTree().GetNetworkUniqueId());

        GetNode<WindowDialog>("WindowMultiplayer").Hide();
        GetNode<Panel>("PnlFindMatch").Show();
    }

    private void _on_BtnAbort_pressed()
    {
        // Tell server we no longer want to find match
        _netState.RpcId(1, nameof(_netState.StopFindMatchRequest));
        // Stop networking
        _netState.Die();

        GetTree().GetRoot().GetNode<Node>("NetState").QueueFree();
        GetNode<Panel>("PnlFindMatch").Hide();
    }


    private void _on_BtnLobbies_pressed()
    {
        GetTree().ChangeScene("res://StageMultiplayer.tscn");
    }

    private void _on_BtnOptions_pressed()
    {
        NotImplementedError();
    }


    private void _on_BtnAbout_pressed()
    {
        NotImplementedError();
    }


    private void _on_BtnQuit_pressed()
    {
        GetTree().Quit();
    }

    private void OnMatchmakingGameStarted()
    {
        Hide();
    }

}




