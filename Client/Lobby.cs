// Lobby script. Client script to manage lobby
using Godot;
using System;
using System.Collections.Generic;

public class Lobby : Node
{

    private Dictionary<int, string> _players = new Dictionary<int, string>();
    public bool Host {get; set;} = false;

    // Only the host can press start. This is also handled server side to prevent others from starting.
    public override void _Ready()
    {
        GetNode<Button>("LobbyUI/BtnStart").Disabled = ! Host;
    }

    // Called by Server to add player to lobby (server handles syncing remember)
    [Remote]
    public void AddPlayer (int ID, string name)
    {

        if (_players.ContainsKey(ID))
            return;

        _players[ID] = name;
        GD.Print("Player (name) added client-side");

        ShowPlayers();
    }

    // Called by server to remove player from lobby
    [Remote]
    private void RemovePlayer(int ID)
    {
        if (_players.ContainsKey(ID))
            _players.Remove(ID);

        ShowPlayers();
    }


    // Refresh the player display
    private void ShowPlayers()
    {
        RichTextLabel rLblPlayers = GetNode<RichTextLabel>("LobbyUI/PnlPlayers/RLblPlayers");
        rLblPlayers.Clear();
        foreach (int ID in _players.Keys)
            rLblPlayers.AddText(_players[ID] + '\n');
    }

    // If Start is pressed, send Rpc to server to request starting the game (for all clients)
    private void _on_BtnStart_pressed()
    {
        RpcId(1, nameof(StartRequest));
    }

    // If server confirms okay to start, it sends Rpc to clients to start the game.
    [Remote]
    private void StartGame()
    {
        PackedScene gameScn = (PackedScene) GD.Load("res://Game.tscn");
        Game game = (Game) gameScn.Instance();
        AddChild(game);
        game.Start();
        GetNode<Control>("LobbyUI").Hide();
        
    }

    // Dummy methods used by server
    public virtual void StartRequest()
    {

    }
}

