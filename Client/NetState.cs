// NetState script. Handles client networking code that generally needs to be globally accessible.
// We connect signals from SceneTree, then we initialise as a client network peer,
// and we handle Rpcs from the server.
// Note how client scripts are a lot less complex than server. We do almost all computations on server.
using Godot;
using System;
using Godot.Collections;

public class NetState : Node
{

    private const string IP = "127.0.0.1";
    private const int PORT = 5000;

    private int _ID;

    public Dictionary<string,string> Lobbies {get; set;} = new Dictionary<string, string>();

    public delegate void ConnectedDelegate();
    public event ConnectedDelegate Connected;
    public delegate void HostedDelegate();
    public event HostedDelegate Hosted;
    public delegate void JoinedDelegate(string lobbyName);
    public event JoinedDelegate Joined;
    public delegate void LobbyListRequestedDelegate();
    public event LobbyListRequestedDelegate LobbyListRequested;

    public delegate void MatchmakingGameStartedDelegate();
    public event MatchmakingGameStartedDelegate MatchmakingGameStarted;

    public override void _Ready()
    {
        GetTree().Connect("connected_to_server", this, nameof(OnConnectedOk));
        GetTree().Connect("connection_failed", this, nameof(OnConnectedFail));
        GetTree().Connect("server_disconnected", this, nameof(OnServerDisconnected));

        ConnectToServer();
    }

    // Initialise as a network peer (IP points to server)
    // To scale, should have list of IPs and join whichever is not saturated
    public void ConnectToServer()
    {
        NetworkedMultiplayerENet host = new NetworkedMultiplayerENet();
        host.CreateClient(IP, PORT);
        GetTree().SetNetworkPeer(host);
        _ID = host.GetUniqueId();
    }

    // Callback from SceneTree, called when connect to server
    public void OnConnectedOk()
    {
        GD.Print("Client ", _ID , " connected to Server");
        // Get updated lobby list from server
        Connected?.Invoke();
    }

    // Callback from SceneTree, called when disconnected from server
    public void OnServerDisconnected()
    {
        // Try to connect again
        ConnectToServer();
    }

    // Callback from SceneTree, called when connection failed
    public void OnConnectedFail()
    {
        // Remove peer
        GetTree().SetNetworkPeer(null);
        // ConnectionFailed?.Invoke();

        // Try to connect again
        ConnectToServer();
    } 

    // End networking for this client
    public void Die()
    {
        GetTree().SetNetworkPeer(null);
        QueueFree();
    }

    // Server tells the client that okay to host.
    // Make the lobby (server handle's other clients)
    // Events raised to manage UI
    [Remote]
    private void HostRequestOk()
    {
        PackedScene lobbyScn = (PackedScene) GD.Load("res://Lobby.tscn");
        Lobby lobby = (Lobby) lobbyScn.Instance();
        lobby.Host = true;
        lobby.SetName("Lobby" + _ID.ToString());
        GetTree().GetRoot().AddChild(lobby);


        Hosted?.Invoke();
    }

    // Server tells the client okay to join, client makes lobby and raises event to manage UI
    [Remote]
    private void JoinRequestOk(string lobbyName)
    {
        PackedScene lobbyScn = (PackedScene) GD.Load("res://Lobby.tscn");
        Lobby lobby = (Lobby) lobbyScn.Instance();
        lobby.SetName(lobbyName);
        GetTree().GetRoot().AddChild(lobby);

        Joined?.Invoke(lobbyName);
    }

    // Get feedback from server if host request fails (not really required yet at this level of complexity)
    [Remote]
    private void HostRequestFailed(string reason)
    {  
        GD.Print("Host request failed.");
        GD.Print("Reason from server: ", reason);
    }

    // Server gives client list of lobbies after client requested it. 
    // Event raised for UI (to display in StageMultiplayer)
    [Remote]
    private void LobbyListRequestOk(Dictionary<string, string> lobbies)
    {
        Lobbies = lobbies;
        LobbyListRequested?.Invoke();
    }

    [Remote]
    private void FindMatchRequestOk()
    {
        MatchmakingGameStarted?.Invoke();
    }

    // Dummy methods used by server
    public virtual void HostRequest(int id)
    {
        
    }
    public virtual void LobbyListRequest(int id)
    {
        
    }
    public virtual void JoinRequest(int id)
    {

    }

    public virtual void ChangeNameRequest(string name)
    {

    }

    public virtual void FindMatchRequest()
    {

    }

    public virtual void StopFindMatchRequest()
    {

    }


}
