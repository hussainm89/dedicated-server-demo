// Game script. Client side script accessed mainly by server to generate, sync, and kill players for each client.
using Godot;
using System;

public class Game : Node2D
{

    private PackedScene _playerScn;

    public override void _Ready()
    {
        _playerScn = (PackedScene) GD.Load("res://Player.tscn");
    }

    public void Start()
    {
        
    }

    // Called by server to instance and initialise new players.
    [Remote]
    private void GeneratePlayer(int ID, Vector2 playerPos, string playerName)
    {
        Player player = (Player) _playerScn.Instance();
        player.Name = "Player" + ID.ToString();
        player.OwnerID = ID;
        AddChild(player);
        player.SetPosition(playerPos);
        GetNode<Label>(player.Name + "/LblName").SetText(playerName);
    }

    // Called by server every frame to give clients updated player positions
    [Remote]
    private void SyncPlayerPosFromServer(string playerNode, Vector2 newPos)
    {
        Player player = GetNode<Player>(playerNode);
        player.SetPosition(newPos);
    }

    // Called by server on attack to sync player colours (colours change on attack)
    [Remote]
    private void SyncPlayerColourFromServer(string playerNode, Color newColour)
    {
        GetNode<Player>(playerNode).SetModulate(newColour);
    }

    // Called by server to kill player locally
    [Remote]
    private void KillPlayerFromServer(string playerNode)
    {
        GetNode<Player>(playerNode).Die();
    }
}
