// StageMultiplayer script. Manages multiplayer UI outside of Lobby.
using Godot;
using System;
using System.Collections.Generic;

public class StageMultiplayer : Control
{



    public int ID {get; set;}
    private NetState _netState;

    private ItemList _iLstLobbies;
    private TextEdit _tEdName;
    private TextEdit _tEdLobbyName;

    private string _myLobbyName = "MyLobby";
    private string _selectedLobby;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        PackedScene netStateScn = (PackedScene)GD.Load("NetState.tscn");
        NetState netState = (NetState) netStateScn.Instance();

        netState.Connected += this.OnConnected;
        netState.LobbyListRequested += this.LobbyListRequestSuccess;
        netState.Hosted += this.HostSuccess;
        netState.Joined += this.JoinSuccess;

        // Add netstate as root node to sync with server (for rpc calls) otherwise gets complicated
        GetTree().GetRoot().AddChild(netState);

        _netState = netState;
        ID = GetTree().GetNetworkUniqueId();

        _iLstLobbies = GetNode<ItemList>("ILstLobbies");
        _tEdName = GetNode<TextEdit>("PnlPlayer/TEdName");
        _tEdLobbyName = GetNode<TextEdit>("PnlPlayer/TEdLobbyName");
        //RefreshLobbyList();
    }

    private void OnConnected()
    {
        RefreshLobbyList();
    }

    private void _on_BtnHost_pressed()
    {
        _netState.RpcId(1, nameof(_netState.HostRequest), ID, _myLobbyName);
    }

    private void HostRequestOk()
    {

    }

    public void HostSuccess()
    {
        GD.Print("Hosted lobby successfully");
        Hide();
    }

    public void JoinSuccess(string lobbyName)
    {
        GD.Print("Joined lobby ", lobbyName, " successfully");
        Hide();
    }

    private void RefreshLobbyList()
    {
        // Ask server for list of lobbies
        _netState.RpcId(1, nameof(_netState.LobbyListRequest), ID);
    }

    // Item list isn't suitable because you can't get index of what you select, or any other hidden data like ID
    public void LobbyListRequestSuccess()
    {
        // Clear lobby list
        _iLstLobbies.Clear();
        // _lobbiesDisplay.Clear();
        int index = 0;
        // Display each lobby on itemlist
        foreach (string lobbyID in _netState.Lobbies.Keys)
        {
            _iLstLobbies.AddItem(_netState.Lobbies[lobbyID]);
            _iLstLobbies.SetItemMetadata(index, lobbyID); // Isn't there a way to find index by string?? otherwise we have to increment index
            index += 1;
        }
    }

    private void _on_BtnJoin_pressed()
    {
        if (_selectedLobby == null)
            return;

        _netState.RpcId(1, nameof(_netState.JoinRequest), ID, _selectedLobby);
    }

    private void _on_BtnRefresh_pressed()
    {
        RefreshLobbyList();
    }

    private void _on_ILstLobbies_item_selected(int index)
    {
        _selectedLobby = (string)_iLstLobbies.GetItemMetadata(index);// GetItemText(index);
        GD.Print("Selected lobby ", _selectedLobby);
    }
    private void _on_TEdName_text_changed()
    {
        _netState.RpcId(1, nameof(_netState.ChangeNameRequest), _tEdName.Text);
    }

    private void _on_TEdLobbyName_text_changed()
    {
        _myLobbyName = _tEdLobbyName.Text;
    }
}






