// Player script. Client side this only handles input and tell's server the result.
using Godot;
using System;
using Godot.Collections;

public class Player : Area2D
{
    public int OwnerID;

    public override void _Ready()
    {
        
    }

    private Array<int> _playerIDsToAttack = new Array<int>();

    public override void _PhysicsProcess(float delta)
    {
        
        if (GetTree().GetNetworkUniqueId() != OwnerID)
            return;

        Vector2 moveDir = new Vector2(0,0);
        bool attacking = false;

        if (Input.IsActionPressed("Up"))
            moveDir.y = -1;
        if (Input.IsActionPressed("Down"))
            moveDir.y = 1;
        if (Input.IsActionPressed("Left"))
            moveDir.x = -1;
        if (Input.IsActionPressed("Right"))
            moveDir.x = 1;

        if (Input.IsActionPressed("Attack"))
            attacking = true;
        
        RpcId(1, nameof(RequestMoveInput), moveDir);
        if (attacking)
            RpcId(1, nameof(RequestAttackInput));

    }

    public void Die()
    {
        QueueFree();
    }

    // Dummy methods used by server
    public virtual void RequestMoveInput(Vector2 moveDir)
    {

    }
    public virtual void RequestAttackInput()
    {

    }
}


